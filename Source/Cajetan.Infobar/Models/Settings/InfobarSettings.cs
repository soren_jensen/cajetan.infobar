﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.Models
{
    public class InfobarSettings
    {
        public List<ModuleSettings> Modules { get; set; }
        public InfobarColors Colors { get; set; }
        public int RefreshInterval { get; set; }

        private InfobarSettings()
        {
        }

        public void Add<T>(T s) where T : ModuleSettings
        {
            if (Modules.Any(e => e.GetType() == (typeof(T))))
                throw new ArgumentException("A module of that type is already loaded!");
            Modules.Add(s);
        }

        public void Remove<T>() where T : ModuleSettings
        {
            Modules.RemoveAll(e => e.GetType() == typeof(T));
        }

        public T Get<T>() where T : ModuleSettings
        { 
            var s = Modules.FirstOrDefault(e => e.GetType() == typeof(T));
            if (s == null)
                s = ModuleSettings.GetDefault<T>();
            return s as T;
        }

        public IEnumerable<ModuleViewModelBase> GetViewModels(ISettingsService settings)
        {
            var modules = Modules.Select(e => e.GetModule(settings));
            return modules;
        }


        #region . Static Default .
        public static InfobarSettings GetDefault()
        {
            return new InfobarSettings
            {
                Modules = new List<ModuleSettings>
                {
                    new UptimeSettings { ShowDays = true },
                    new BatterySettings { ShowTime = true },
                    new ProcessorSettings {},
                    new MemorySettings {},
                    new NetworkSettings {}
                },
                Colors = new InfobarColors(),
                RefreshInterval = 500
            };
        }
        #endregion
    }
}
