﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Cajetan.Infobar.Models
{
    public class InfobarColors
    {
        public string Background { get; set; }
        public string Foreground { get; set; }
        public string Border { get; set; }

        public InfobarColors()
        {
            Background = "#FF3B3B3B";
            Foreground = "#FFF5F5F5";
            Border = "#FF5E6F7F";
        }

        #region . Static Convertion Methods .
        public static SolidColorBrush FromHex(string hex)
        {
            var c = new BrushConverter();
            return (c.ConvertFromString(hex) as SolidColorBrush);
        }

        public static string FromBrush(SolidColorBrush color)
        {
            var c = new BrushConverter();
            return (c.ConvertToString(color) as string);
        }

        public static SolidColorBrush Invert(SolidColorBrush c, int offset = -25)
        {
            byte MAX = 255;
            byte r = Convert.ToByte(MAX - c.Color.R);
            byte g = Convert.ToByte(MAX - c.Color.G);
            byte b = Convert.ToByte(MAX - c.Color.B);
            var inv = Color.FromRgb(r, g, b);
            if (inv.B + offset > 0)
                inv.B = Convert.ToByte(inv.B + offset);
            return new SolidColorBrush(inv);
        }

        public static SolidColorBrush Invert(string hex, int offset = -10)
        {
            return Invert(FromHex(hex), offset);
        }
        #endregion
    }
}
