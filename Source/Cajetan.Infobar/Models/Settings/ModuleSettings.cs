﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.Models
{
    public abstract class ModuleSettings
    {
        public abstract ModuleViewModelBase GetModule(ISettingsService settings);

        public static T GetDefault<T>() where T : ModuleSettings
        {
            var type = typeof(T);

            if (type == typeof(BatterySettings))
                return new BatterySettings { ShowTime = true } as T;

            else if (type == typeof(MemorySettings))
                return new MemorySettings { } as T;

            else if (type == typeof(NetworkSettings))
                return new NetworkSettings { } as T;

            else if (type == typeof(ProcessorSettings))
                return new ProcessorSettings { } as T;

            else if (type == typeof(UptimeSettings))
                return new UptimeSettings { ShowDays = false } as T;

            else
                return default(T);
        }
    }

    public class BatterySettings : ModuleSettings
    {
        public bool ShowTime { get; set; }

        public override ModuleViewModelBase GetModule(ISettingsService settings)
        {
            return new BatteryStatusViewModel(settings);
        }
    }


    public class MemorySettings : ModuleSettings
    {
        public override ModuleViewModelBase GetModule(ISettingsService settings)
        {
            return new MemoryUsageViewModel(settings);
        }
    }


    public class NetworkSettings : ModuleSettings
    {
        public override ModuleViewModelBase GetModule(ISettingsService settings)
        {
            return new NetworkUsageViewModel(settings);
        }
    }


    public class ProcessorSettings : ModuleSettings
    {
        public override ModuleViewModelBase GetModule(ISettingsService settings)
        {
            return new ProcessorUsageViewModel(settings);
        }
    }


    public class UptimeSettings : ModuleSettings
    {
        public bool ShowDays { get; set; }

        public override ModuleViewModelBase GetModule(ISettingsService settings)
        {
            return new UptimeViewModel(settings);
        }
    }
}
