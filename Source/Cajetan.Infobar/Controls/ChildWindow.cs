﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Cajetan.Infobar.Controls
{
    /// <summary>
    /// A styled window.
    /// </summary>
    public class ChildWindow : Window
    {
        static ChildWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ChildWindow), new FrameworkPropertyMetadata(typeof(ChildWindow)));
        }

        public ChildWindow()
        {
            AllowsTransparency = true;
            WindowStyle = WindowStyle.None;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Panel titleBar = GetTemplateChild("PART_TitleBar") as Panel;
            titleBar.MouseDown += TitleBar_MouseDown;

            Button closeButton = GetTemplateChild("PART_CloseButton") as Button;
            closeButton.Click += CloseButton_Click;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
            }
        }
    }

}
