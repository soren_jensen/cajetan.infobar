﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public abstract class ModuleViewModelBase : ViewModelBase
    {
        protected ISettingsService _settings;

        public abstract void UpdateData(SystemInfoProvider info);

        public ModuleViewModelBase(ISettingsService settings)
        {
            _settings = settings;
        }
    }
}
