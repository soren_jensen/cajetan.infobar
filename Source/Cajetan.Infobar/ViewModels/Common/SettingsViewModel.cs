﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        private ISettingsService _settings;
        private IWindowService _windowService;

        public SettingsViewModel(ISettingsService settings, IWindowService windowService)
        {
            DisplayName = "Cajetan Infobar - Settings";

            _settings = settings;
            _windowService = windowService;
        }

        
    }
}
