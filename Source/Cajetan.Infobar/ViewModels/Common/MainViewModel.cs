﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Cajetan.Core;
using Cajetan.Core.AppBar;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private AppBarController _ctrl;
        private SystemInfoProvider _info;
        private DispatcherTimer _timer;

        private ISettingsService _settings;
        private IWindowService _windowService;

        private readonly ObservableCollection<ModuleViewModelBase> _modules = new ObservableCollection<ModuleViewModelBase>();
        public ObservableCollection<ModuleViewModelBase> Modules { get { return _modules; } }

        public string BackgroundHex { get { return _settings.Current.Colors.Background; } }
        public string ForegroundHex { get { return _settings.Current.Colors.Foreground; } }
        public string BorderHex { get { return _settings.Current.Colors.Border; } }

        public MainViewModel(AppBarController ctrl, ISettingsService settings, IWindowService windowService)
        {
            _ctrl = ctrl;
            _settings = settings;
            _windowService = windowService;

            _info = new SystemInfoProvider();

            _timer = new DispatcherTimer(DispatcherPriority.SystemIdle);
            _timer.Tick += _timer_Tick;
            _timer.Interval = TimeSpan.FromSeconds(1);

            // Load Settings
            _settings.Load();
            foreach (var ms in _settings.Current.GetViewModels(_settings))
                _modules.Add(ms);

            // Start timer
            _timer.Start();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            _info.UpdateInfo();
            foreach (var m in Modules)
            {
                m.UpdateData(_info);
            }
        }
        
        public ICommand ResetCommand { get { return new RelayCommand(param => this.Reset()); } }
        public void Reset()
        {
            var edge = _ctrl.CurrentEdge;
            _ctrl.Undock();
            if (edge == AppBarController.ABEdge.Bottom)
                _ctrl.DockBottom();
            else if (edge == AppBarController.ABEdge.Top)
                _ctrl.DockTop();
            else
                _ctrl.DockNone();
        }

        public ICommand DockCommand { get { return new RelayCommand(param => this.Dock()); } }
        public void Dock()
        {
            _ctrl.DockBottom();
        }

        public ICommand CloseCommand { get { return new RelayCommand(param => this.Close()); } }
        public void Close()
        {
            // Save Settings
            _settings.Save();

            // Unregister AppBar
            _ctrl.Undock();

            // Shutdown application
            Application.Current.Shutdown();
        }

        public ICommand OpenSettingsCommand { get { return new RelayCommand(param => this.OpenSettings()); } }
        public void OpenSettings()
        {
            var s = _windowService.OpenDialog(new SettingsViewModel(_settings, _windowService));
            if (s.HasValue && s.Value)
            {

            }
        }
    }
}
