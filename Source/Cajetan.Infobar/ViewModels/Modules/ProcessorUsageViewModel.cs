﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class ProcessorUsageViewModel : ModuleViewModelBase
    {

        private string _usage;
        public string Usage
        {
            get { return _usage; }
            set { if (_usage != value) { _usage = value; RaisePropertyChanged(() => this.Usage); } }
        }

        private readonly ObservableCollection<int> _values = new ObservableCollection<int>();
        public ObservableCollection<int> Values { get { return _values; } }

        public ProcessorUsageViewModel(ISettingsService settings)
            : base(settings)
        {

        }


        public override void UpdateData(SystemInfoProvider info)
        {
            Usage = info.ProcessorUsageString;
            Values.Add(info.ProcessorUsage);
        }
    }
}
