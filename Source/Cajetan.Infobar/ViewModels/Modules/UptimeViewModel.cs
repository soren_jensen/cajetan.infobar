﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class UptimeViewModel : ModuleViewModelBase
    {
        private UptimeSettings _uptimeSettings;

        public bool ShowDays
        {
            get { return _uptimeSettings.ShowDays; }
            set { if (_uptimeSettings.ShowDays != value) { _uptimeSettings.ShowDays = value; RaisePropertyChanged(() => this.ShowDays); } }
        }

        private string _uptime;
        public string Uptime
        {
            get { return _uptime; }
            set { if (_uptime != value) { _uptime = value; RaisePropertyChanged(() => this.Uptime); } }
        }

        public UptimeViewModel(ISettingsService settings)
            : base(settings)
        {
            _uptimeSettings = _settings.Current.Get<UptimeSettings>();
        }

        public override void UpdateData(SystemInfoProvider info)
        {
            Uptime = info.UptimeString;
        }
    }
}
