﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class BatteryStatusViewModel : ModuleViewModelBase
    {
        private BatterySettings _battery;

        public bool ShowTime
        {
            get { return _battery.ShowTime; }
            set { if (_battery.ShowTime != value) { _battery.ShowTime = value; RaisePropertyChanged(() => this.ShowTime); } }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { if (_status != value) { _status = value; RaisePropertyChanged(() => this.Status); } }
        }

        public BatteryStatusViewModel(ISettingsService settings)
            : base(settings)
        {
            _battery = settings.Current.Get<BatterySettings>();
        }

        public override void UpdateData(SystemInfoProvider info)
        {
            Status = info.BatteryStatusString;
        }

    }
}
