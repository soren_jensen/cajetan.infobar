﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class NetworkUsageViewModel : ModuleViewModelBase
    {

        private string _upload;
        public string Upload
        {
            get { return _upload; }
            set { if (_upload != value) { _upload = value; RaisePropertyChanged(() => this.Upload); } }
        }

        private string _download;
        public string Download
        {
            get { return _download; }
            set { if (_download != value) { _download = value; RaisePropertyChanged(() => this.Download); } }
        }

        public NetworkUsageViewModel(ISettingsService settings)
            : base(settings)
        {

        }

        public override void UpdateData(SystemInfoProvider info)
        {
            Download = info.NetworkDownloadString;
            Upload = info.NetworkUploadString;
        }
    }
}
