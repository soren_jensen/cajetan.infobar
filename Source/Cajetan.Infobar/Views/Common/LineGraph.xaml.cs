﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cajetan.Infobar.Views
{
    /// <summary>
    /// Interaction logic for LineGraph.xaml
    /// </summary>
    public partial class LineGraph : UserControl
    {
        public static readonly DependencyProperty ValuesProperty = DependencyProperty.Register("Values", typeof(ObservableCollection<int>), typeof(LineGraph), new PropertyMetadata((o, e) => { ((LineGraph)o).ValuesChanged(); }));
        public ObservableCollection<int> Values
        {
            get { return (ObservableCollection<int>)GetValue(ValuesProperty); }
            set { SetValue(ValuesProperty, value); }
        }
        private void ValuesChanged()
        {
            if (this.Values != null)
            {
                this.Values.CollectionChanged += (s, e) =>
                {
                    foreach (var i in e.NewItems)
                        AddValue((int)i);
                };
            }
        }

        private double _shift = 1.3;
        private int _offset = 0;
        public int Offset { get { return _offset; } set { _offset = value; } }

        private bool _valueShift = false;
        public bool UseValueShift { get { return _valueShift; } set { _valueShift = value; } }

        private int _width = 0;
        private int _height = 0;

        private List<int> _values;

        public LineGraph()
        {
            InitializeComponent();
            _values = new List<int>();
        }

        /// <summary>
        /// Add a number to the list of values. If number of items exceed maximum number of allowed items the oldest is removed.
        /// </summary>
        /// <param name="val">An integer between 0 and 100.</param>
        private void AddValue(int val)
        {
            if (_width > 0)
            {
                if (val < 0)
                    val = 0;
                if (val > 100)
                    val = 100;

                if (_values.Count >= _width / 2)
                    _values.RemoveAt(0);
                _values.Add(val);

                UpdateGraph();
            }
        }

        private void UpdateGraph()
        {
            List<Point> p = new List<Point>();
            List<Point> t = new List<Point>();

            int startX = _width + 2;
            int startY = _height - 1;

            int n = _values.Count * 2;
            foreach (var v in _values)
            {
                int x = startX - n;
                int y = startY - MapValueToRange(v);
                t.Add(new Point(x, y));
                n -= 2;
            }

            if (t.Count > 0)
            {
                var v = t[0].Y;
                for (int i = 0; i < _width - (_values.Count * 2); i += 2)
                {
                    p.Add(new Point(i, v));
                }
                p.AddRange(t);
            }

            //Debug.WriteLine(this.Tag + ": " + _values.Last() + " => " + p.Last().Y);

            line.Points = new PointCollection(p);
        }

        /// <summary>
        /// Maps a percentage value to the range "0 to _height"
        /// </summary>
        private int MapValueToRange(int value)
        {
            // Calc percentage value as decimal between 0 & 1
            double p = ((double)value) / 100;
            if (_valueShift)
            {
                // Calc value shift and apply to p
                double c = _shift - p;
                p = p * c;
            }
            // Calc percentage of height
            double v = _height * p;
            // Round up
            v = Math.Ceiling(v);

            // Apply offset
            v = v + Offset;
            // Correct if v is out-of-bounds (not between 0-15)
            if (v > _height) { v = _height; }
            else if (v < 0) { v = 0; }

            // Return Int32 converted value.
            return Convert.ToInt32(v);

        }

        /// <summary>
        /// EventHandler for Loaded event. Sets width and height.
        /// </summary>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _width = Convert.ToInt32(this.ActualWidth);
            _height = Convert.ToInt32(this.ActualHeight);
        }

        /// <summary>
        /// EventHandler for SizeChanged event. Updates width and height variables with new values.
        /// </summary>
        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _width = Convert.ToInt32(this.ActualWidth);
            _height = Convert.ToInt32(this.ActualHeight);
        }


    }
}
