﻿using System;
using Cajetan.Core.Mvvm;

namespace Cajetan.Infobar.Services
{
    public interface IWindowService
    {
        bool? OpenDialog(ViewModelBase viewModel);
        bool? OpenDialog(ViewModelBase viewModel, bool allowResize);
        bool? OpenDialog(ViewModelBase viewModel, bool allowResize, Action loadedAction);

        void OpenWindow(ViewModelBase viewModel);
        void OpenWindow(ViewModelBase viewModel, bool allowResize);
        void OpenWindow(ViewModelBase viewModel, bool allowResize, Action loadedAction);

        void CloseWindow(ViewModelBase viewModel);
        void CloseWindow(ViewModelBase viewModel, bool result);
    }
}
