﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.ViewModels;


namespace Cajetan.Infobar.Services
{
    public class SettingsService : ISettingsService
    {
        #region . XML Serializer .
        private XmlSerializer<InfobarSettings> _xml;
        private Type[] _types = new[]
        {
            typeof(InfobarColors),
            typeof(MemorySettings),
            typeof(UptimeSettings),
            typeof(BatterySettings),
            typeof(NetworkSettings),
            typeof(ProcessorSettings),
            typeof(ModuleSettings),
        };
        private string _file;
        #endregion


        public InfobarSettings Current { get; set; }

        public event EventHandler SettingsChanged;
        private void RaiseSettingsChanged()
        {
            if (SettingsChanged != null)
                SettingsChanged(this, EventArgs.Empty);
        }


        public SettingsService()
        {
            _xml = new XmlSerializer<InfobarSettings>(_types);
            _file = "Settings.xml";
        }


        public void Load()
        {
            InfobarSettings s = null;
            try { s = _xml.Load(_file); } catch { }

            if (s == null)
            {
                Current = InfobarSettings.GetDefault();
                Save();
            }
            else
            {
                Current = s;
            }
        }

        public void Save()
        {
            try
            {
                _xml.Save(_file, Current);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Unable to save settings!", ex);
            }
        }

    }
}
