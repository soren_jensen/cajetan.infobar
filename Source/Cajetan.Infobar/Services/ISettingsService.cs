﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.Services
{
    public interface ISettingsService
    {
        InfobarSettings Current { get; set; }

        event EventHandler SettingsChanged;

        void Load();
        void Save();
    }
}
