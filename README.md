Continuation of my earlier InfoBar project (https://bitbucket.org/reptail/infobar). 

Starting from scratch using WPF (Windows Presentation Framework), trying to build it as generic and extendible as possible. 

2014-04-08: Renamed to "Cajetan.Infobar", and code converted to follow the MVVM pattern. Caused loss of old history.